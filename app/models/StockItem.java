package models;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by wellfrog on 2/8/2015.
 */
@Entity
public class StockItem extends Model{
    public static Finder<Long, StockItem> find = new Finder<>(Long.class, StockItem.class);
    @Id
    public Long id;
    @ManyToOne
    public Warehouse warehouse;
    @ManyToOne
    public Product product;
    public Long quantity;
    public String toString(){
        return String.format("StockItem %d - %d x product %s", id, quantity, product == null ? null : product.id);
    }

}

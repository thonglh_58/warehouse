package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wellfrog on 2/8/2015.
 */
@Entity
public class Warehouse {
    public static Model.Finder<Long, Warehouse> find = new Model.Finder<>(Long.class, Warehouse.class);
    @Id

    public Long id;

    @OneToOne
    public Address address;
    public String name;



    @OneToMany(mappedBy = "warehouse")

    public List<StockItem> stock = new ArrayList();
}

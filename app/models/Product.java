package models;

/**
 * Created by wellfrog on 2/6/2015.
 */

import com.avaje.ebean.Page;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.mvc.PathBindable;
import play.mvc.Result;
import play.db.ebean.Model;
import javax.persistence.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.text.Format;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
@Entity

public class Product extends Model implements  play.mvc.PathBindable<Product> {


    private static List<Product> products;

    static {

        products = new ArrayList<Product>();

        products.add(new Product("1111111111111", "Paperclips 1",

                "Paperclips description 1"));

        products.add(new Product("2222222222222", "Paperclips 2",

                "Paperclips description 2"));

        products.add(new Product("3333333333333", "Paperclips 3",

                "Paperclips description 3"));

        products.add(new Product("4444444444444", "Paperclips 4",

                "Paperclips description 4"));

        products.add(new Product("5555555555555", "Paperclips 5",

                "Paperclips description 5"));

    }
    @Id
    public Long id;
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;
    public String description;
    //public byte[] picture;


    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();
    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;


    public Product() {}

    public Product(String ean, String name, String description) {

        this.ean = ean;

        this.name = name;

        this.description = description;

    }
    public static Finder<Long,Product> find= new Finder<>(Long.class,Product.class);
    public String toString() {

        return String.format("%s - %s - %s", ean, name,tags.toString());

    }
    public static List<Product> findAll() {

        return find.all();

    }
    private void showDatabase(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Connection connection = null;
        try {
            connection = getConnection();

            Statement stmt = connection.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS ticks (tick timestamp)");
            stmt.executeUpdate("INSERT INTO ticks VALUES (now())");
            ResultSet rs = stmt.executeQuery("SELECT tick FROM ticks");

            String out = "Hello!\n";
            while (rs.next()) {
                out += "Read from DB: " + rs.getTimestamp("tick") + "\n";
            }

            resp.getWriter().print(out);
        } catch (Exception e) {
            resp.getWriter().print("There was an error: " + e.getMessage());
        } finally {
            if (connection != null) try{connection.close();} catch(SQLException e){}
        }
    }
    public Connection getConnection() throws URISyntaxException, SQLException {
        //URI dbUri = new URI(System.getenv("DATABASE_URL"));

        String username = "mtfvybzalotbzv";
        String password = "osddtWBi6csSruE9prxCNeqqL6";
        int port = 5432;

        String dbUrl = "jdbc:postgresql://" + "ec2-23-21-154-37.compute-1.amazonaws.com" + ":" + port + "dehrkk8vap828g";

        return DriverManager.getConnection(dbUrl, username, password);
    }




    public static Product findByEan(String ean) {

        return find.where().eq("ean", ean).findUnique();

    }



    public static List<Product> findByName(String term) {

        final List<Product> results = new ArrayList<Product>();

        for (Product candidate : products) {

            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {

                results.add(candidate);

            }

        }

        return results;

    }
    public static Page<Product> find(int page) {  // trả về trang thay vì List

        return find.where()

                .orderBy("id asc")     // sắp xếp tăng dần theo id

                .findPagingList(5)    // quy định kích thước của trang

                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?

                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0

    }


    @Override
    public Product bind(String s, String s1) {
        return findByEan(s1);
    }

    @Override
    public String unbind(String s) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }
}

package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by wellfrog on 2/8/2015.
 */
@Entity
public class Tag {
    @Id
    public Long id;
    public String name;
    @ManyToMany(mappedBy="tags")
    public List<Product> products = new LinkedList<Product>();
    public static Model.Finder<Long, Tag> find = new Model.Finder<>(Long.class, Tag.class);
    public static Tag findById(Long id) {

        return find.byId(id);

    }
}